let webpackConfig = require('../webpack.config.babel.js');

//delete webpackConfig.entry;
//delete webpackConfig.output;

module.exports = (storybookBaseConfig /*, configType*/) => {
    //storybookBaseConfig.module.rules.push(webpackConfig.module.loaders[0]);
    storybookBaseConfig.resolve.alias = webpackConfig.resolve.alias;
    //storybookBaseConfig.resolve.modules = webpackConfig.resolve.modules;
    return storybookBaseConfig;
};

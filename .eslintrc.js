module.exports = {
    "env": {
        "browser": true,
        "es6": true,
        "jquery": true
    },
    "globals": {
        // For backbone so it won't error for me (there is probably a better way to do this)
        "el": true,
        "$el": true,
        "render": true,
        "global": true,
        "require": true,
        "module": true
    },
    "extends": "eslint:recommended",
    "parserOptions": {
        "ecmaFeatures": {
            "experimentalObjectRestSpread": true,
            "jsx": true
        },
        "sourceType": "module"
    },
    "plugins": [
        "react",
        "backbone"
    ],
    "rules": {
        "indent": [
            "error",
            4
        ],
        "linebreak-style": [
            "error",
            "unix"
        ],
        "quotes": [
            "error",
            "single",
            {allowTemplateLiterals: true}
        ],
        "semi": [
            "error",
            "always"
        ],
        "no-console": [
            "error",
            {
                allow: ["log", "warn", "error"]
            }
        ],

        // React Rules
        "react/jsx-uses-react": "error",
        "react/jsx-uses-vars": "error"
    }
};

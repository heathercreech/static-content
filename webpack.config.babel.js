var webpack = require('webpack');
var path = require('path');
var sync = require('webpack-livereload-plugin');
var globImport = require('babel-plugin-import-glob');
var yaml = require('yamljs');
var BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

const APP_CONFIG_PATH = __dirname + '/config/apps.yaml';

// List static assets that are separated into pyramid apps
const BASE_APP_PATH = __dirname + '/../';
const APP_STATIC_PATH = '/static/js/';
const APPS = yaml.load(APP_CONFIG_PATH);

// Normal entries
let entries = {
    'vendor':       './js/vendor.js'
};

// Map the app entries
for (let appName in APPS) {
    let appConfig = APPS[appName];
    entries[appName] = BASE_APP_PATH + appName + '/' + APP_STATIC_PATH + appConfig.entry;
}

// Since we have assets built from app directories,
// we need to supply absolute paths for the babel plugins
// and presets
let modulesPath = __dirname + '/node_modules';
let pluginPrefix = modulesPath + '/babel-plugin-';
let presetPrefix = modulesPath + '/babel-preset-';

let plugin = name => (pluginPrefix + name);
let preset = name => (presetPrefix + name);


module.exports = {
    context: __dirname + "/src",
    resolve: {
        alias: {
            app: path.resolve(__dirname, 'src', 'js', 'apps'),
            lib: path.resolve(__dirname, 'src', 'js', 'lib'),
            visual: path.resolve(__dirname, 'src', 'js', 'visual'),
            logic: path.resolve(__dirname, 'src', 'js', 'logic')
        },
        modules: [path.join(__dirname, 'node_modules')]
    },
    resolveLoader: {
        modules: [path.join(__dirname, 'node_modules')]
    },
    entry: entries,
    output: {
        path: __dirname + "/public/js",
        filename: '[name].js'
    },
    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                exclude: /(node_modules)/,
                loader: 'eslint-loader',
                enforce: 'pre',
                options: {
                    configFile: '.eslintrc.js'
                }
            },
            {
                test: /\.jsx?$/,
                exclude: /(node_modules)/,
                loader: 'babel-loader',
                query: {
                    presets: [preset('es2015'), preset('react')],
                    plugins: [
                        plugin('transform-es2015-parameters'),
                        plugin('transform-object-rest-spread'),
                        [plugin('import-glob')]
                    ]
                }
            },
            {
                test: /\.gql$/,
                exclude: /(node_modules)/,
                loader: 'raw-loader'
            }
        ]
    },
    plugins: [
        new sync({
            hostname: 'localhost'
        }),
        //new BundleAnalyzerPlugin(),
        new webpack.optimize.CommonsChunkPlugin('init'),
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify('production')
            }
        }),
        new webpack.optimize.UglifyJsPlugin(),
        new webpack.optimize.AggressiveMergingPlugin()
    ],
    node: {
        fs: "empty"
    },
    stats: {
        assets: false,
        version: false,
        hash: false,
        chunks: false,
        chunkModules: false,
        timings: true,
        colors: true
    },
    devtool: '#source-map'
}

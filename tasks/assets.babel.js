module.exports = function(gulp, plugins, assetsContext) {
    let {build} = plugins;

    gulp.task('assets', function() {
        return build(gulp, plugins, this, assetsContext, false)
            .pipe(gulp.dest(assetsContext.target));
    });
}

module.exports = function(gulp, plugins, tpContext) {
    let {build} = plugins;

    gulp.task('third-party', function() {
        return build(gulp, plugins, this, tpContext, false)
            .pipe(gulp.dest(tpContext.target));
    });
}

module.exports = function(gulp, plugins, jsContext) {
    let {build, webpack, webpackDev, del, options, refresh, vinyl} = plugins;

    gulp.task('js', function() {
        let conf = require('../webpack.config.babel.js');

        let res = build(gulp, plugins, this, jsContext)
            .pipe(vinyl())
            .pipe(webpack(conf))
            .pipe(gulp.dest(jsContext.target));

        if (options.has('watch')) {
            res = res.pipe(refresh());
        }

        return res;
    });

    gulp.task('clean:js', function() {
        return del([
            jsContext.target + '/**/*'
        ]);
    });
}

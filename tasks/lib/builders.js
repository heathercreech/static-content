//Capitalize first letter of string
String.prototype.formal = function() {
    if (this.length > 0) {
        return this.charAt(0).toUpperCase() + this.slice(1);
    } else {
        return this;
    }
};

export default function(gulp, plugins, that, taskContext, enableDebug=true) {
    let {debug, options, browser} = plugins;

    let taskName = that.seq.slice(-1)[0];
    let buildStream = gulp.src(taskContext.glob);
    if (options.has('watch') && !taskContext.disableWatch) {
        if (!options.has(taskName)) {
            let actualGlob = (taskContext.watchGlob.length > 0) ? taskContext.watchGlob : taskContext.glob;
            gulp.watch(actualGlob, [that.taskName]);
            options[taskName] = true;
        }
    }

    if (enableDebug) {
        var debugTitle = that.seq.slice(-1)[0].formal();
        debugTitle += ': ';

        buildStream = buildStream.pipe(debug({title: debugTitle}));
    }

    return buildStream;

};

module.exports = function(gulp, plugins, sassContext, htmlContext) {
    let {build, sass, sassBulk, nano, del, refresh} = plugins;

    gulp.task('sass', function() {
        let buildStream = build(gulp, plugins, this, sassContext);
        return buildStream.pipe(sassBulk())
            .pipe(sass({includePaths: ['src/sass']})
                .on('error', sass.logError)
            )
            //.pipe(nano())
            .pipe(gulp.dest(sassContext.target))
            .pipe(refresh());
    });

    gulp.task('clean:sass', function() {
        return del([
            sassContext.target + '/**/*'
        ]);
    });
}

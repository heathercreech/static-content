module.exports = function(gulp, plugins, htmlContext) {
    let {build, nunjucks, del, refresh} = plugins;

    let nunjucksConfig = function(environment) {
        environment.addGlobal('uuid', require('uuid'));
    }

    gulp.task('html', function() {
        return build(gulp, plugins, this, htmlContext)
            .pipe(nunjucks({
                path: 'src/templates',
                manageEnv: nunjucksConfig
            }))
            .pipe(gulp.dest(htmlContext.target))
            .pipe(refresh())
    });

    gulp.task('clean:html', function() {
        return del([
            htmlContext.target + '/**/*'
        ]);
    });
}

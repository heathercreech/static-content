import build from './tasks/lib/builders.js';
var gulp = require('gulp');
var plugins = {
    build: build,
    del: require('del'),
    watch: require('gulp-watch'),
    debug: require('gulp-debug'),
    options: require('gulp-options'),
    webpack: require('webpack-stream'),
    webpackDev: require('webpack-dev-server'),
    nunjucks: require('gulp-nunjucks-render'),
    sass: require('gulp-sass'),
    sassBulk: require('gulp-sass-bulk-import'),
    nano: require('gulp-cssnano'),
    subtasks: require('gulp-load-subtasks'),
    refresh: require('gulp-livereload'),
    vinyl: require('vinyl-named'),
    yaml: require('yamljs'),
}

var _runTask = gulp.Gulp.prototype._runTask;
gulp.Gulp.prototype._runTask = function(task) {
    this.taskName = task.name;
    _runTask.apply(this, arguments);
};

var globSrc = 'src/'; //prepends the other glob strings
var outputDir = 'public/';

class TaskContext {
    constructor(glob, target, watchGlob='', disableWatch=false) {
        this.glob = (Array.isArray(glob)) ? glob : globSrc + glob;
        this.watchGlob = (watchGlob.length > 0) ? globSrc + watchGlob : this.glob;
        this.target = outputDir + target;
        this.disableWatch = disableWatch; //Allows overriding of default watch behaviour
    }
}


const APP_CONFIG_PATH = __dirname + '/config/apps.yaml';

// Get our config which contains a list of static assets
// that are separated into pyramid apps
const BASE_APP_PATH = __dirname + '/../';
const APP_STATIC_PATH = '/static/';
const APPS = plugins.yaml.load(APP_CONFIG_PATH);


// Get our scss entry points
let sassEntries = ['src/sass/combined.scss']
for (let appName in APPS) {
    let appConfig = APPS[appName];
    if (appConfig.sass) {
        sassEntries.push(BASE_APP_PATH + appName + '/' + appName + APP_STATIC_PATH + 'sass/' + appConfig.sass.entry);
    }
}


let htmlContext = new TaskContext('templates/**/*.html', 'templates');
let jsContext = new TaskContext(['src/js/main.js', 'src/js/vendor.js'], 'js', 'js/**/*.{js,jsx}');
let sassContext = new TaskContext(sassEntries, 'css', 'sass/**/*.scss');
let assetsContext = new TaskContext('assets/**/', 'assets');
let tpContext = new TaskContext('third-party/**/', '');

let {subtasks, options, refresh} = plugins;
subtasks('tasks/html.babel.js', plugins, htmlContext);
subtasks('tasks/sass.babel.js', plugins, sassContext, htmlContext);
subtasks('tasks/js.babel.js', plugins, jsContext);
subtasks('tasks/assets.babel.js', plugins, assetsContext);
subtasks('tasks/third-party.babel.js', plugins, tpContext);

if (options.has('watch')) {
    refresh.listen();
}

gulp.task('default', ['js', 'sass', 'html']);

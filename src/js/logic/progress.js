import React from 'react';


export class ProgressBar extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let {height, progress, label, background, fill, className} = this.props;
        if (!label) {
            label = progress.toFixed(2) + '%';
        }

        return (
            <div
                className={'relative full-width full-height border-bottom-thin ' + className + ' ' + background}
                style={(height) ? {height: height} : {}}
                onClick={this.onClick.bind(this)}
            >
                <div className={'absolute full-width text-center'} style={{'height': '90%'}}>
                    <div className="flex-container align-center full-height">
                        <div className="flex-sm-12 text-center">
                            {label}
                        </div>
                    </div>
                </div>
                <div
                    className={'full-height ' + fill}
                    style={{'width': progress + '%', transition: 'width .1s'}}>
                </div>
            </div>
        );
    }

    onClick(e) {
        let element = e.target;
        let dimensions = element.getBoundingClientRect();
        let x = e.clientX - dimensions.left;

        if(this.props.onClick) {
            this.props.onClick(x/element.offsetWidth);
        }
    }
}

import React from 'react';
import ReactDOM from 'react-dom';

import { createStore, combineReducers, applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger';
import thunkMiddleware from 'redux-thunk';
import { Provider } from 'react-redux';

import { Router as Router, Route, Switch } from 'react-router';
import { createBrowserHistory } from 'history';
import { routerReducer, routerMiddleware } from 'react-router-redux';
import { Page } from 'visual/page';
import { ErrorBoundary } from 'logic/error';


const loggerMiddleware = createLogger();


export function renderPage(appRoutes, reducers, colors, selector) {
    if (typeof(selector) == 'undefined') {
        selector = '#one-page-container';
    }

    let $el = $(selector);
    if ($el.length) {
        ReactDOM.render(
            <OnePageApp appRoutes={appRoutes} reducers={reducers} colors={colors} />,
            $el[0]
        );
        return undefined;
    }
    console.error('Unable to find one-page container with supplied/default selector.');
}


export class OnePageApp extends React.Component {
    constructor(props) {
        super(props);
        this.history = createBrowserHistory();
        this.store = createStore(
            combineReducers({...props.reducers, routing: routerReducer}),
            applyMiddleware(thunkMiddleware, loggerMiddleware, routerMiddleware(this.history))
        );
    }

    render() {
        let {appRoutes, colors} = this.props;
        return (
            <ErrorBoundary>
                <Page className={colors}>
                    <Provider store={this.store}>
                        <Router history={this.history}>
                            <Switch>
                                {appRoutes.map((appRoute, index) =>
                                    <Route
                                        key={index}
                                        path={appRoute.path}
                                        component={appRoute.app}
                                    />
                                )}
                            </Switch>
                        </Router>
                    </Provider>
                </Page>
            </ErrorBoundary>
        );
    }
}

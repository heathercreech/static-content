import React from 'react';
import {Howl} from 'howler';


export class Audio extends React.Component {
    constructor(props) {
        super(props);
        let { src, html5=true, preload=false } = this.props;

        this.audio = new Howl({
            src, html5, preload,
            onload: this.onLoad.bind(this),
            onplay: this.onPlay.bind(this),
            onpause: this.onPause.bind(this),
            onseek: this.onSeek.bind(this),
            onend: this.onEnd.bind(this)
        });

        this.state = {
            playing: false,
            loaded: false,
            position: 0
        };

    }

    render() {
        let {children} = this.props;
        return children(this.state, this.audio);
    }

    onLoad() {
        let { onLoad } = this.props;
        this.setState({ loaded: true });
        if (onLoad) onLoad();
    }

    onPlay() {
        let { onPlay } = this.props;
        this.trackPosition();
        this.setState({ playing: true });
        if (onPlay) onPlay();
    }

    onPause() {
        let { onPause } = this.props;
        this.untrackPosition();
        this.setState({ playing: false });
        if (onPause) onPause();
    }

    onEnd() {
        let { onEnd } = this.props;
        this.untrackPosition();
        this.setState({ playing: false, position: 0 });
        if (onEnd) onEnd();
    }

    onSeek() {
        let { onSeek } = this.props;
        this.setState({ position: this.audio.seek() });
        if (onSeek) onSeek();
    }

    trackPosition() {
        this.posEvent = setInterval(
            () => {
                if (this.state.loaded) {
                    this.setState({position: this.audio.seek()});
                }
            },
            500
        );
    }

    untrackPosition() {
        clearInterval(this.posEvent);
    }
}

import React from 'react';
import Howler from 'howler';

import {TimerDisplay} from './timer-display.jsx';
import {TimerControls} from './timer-controls.jsx';

export class Timer extends React.Component {
    constructor() {
        super();
        this.alarm = new Howler.Howl({
            src: ['/static/assets/audio/alarm.mp3'],
            loop: true
        });
        this.state = {
            started: false, // If the timer is in the middle of a countdown
            active: false, // If the timer is currently counting down
            time: {minutes: 3, seconds: 0}, // The timers current value
            stored_time: {minutes: 3, seconds: 0} // Used to tell what the timer was started at
        };
    }

    render() {
        let {active, started, time} = this.state;
        return (
            <div className="page-width page-height twilight text-aluminum display-table text-center text-billboard relative">
                <TimerControls
                    active={active}
                    start={() => {if (time.minutes != 0 || time.seconds != 0) {this.toggleTimer();} }}
                    reset={() => {this.resetTimer(); this.alarm.stop();}}
                />
                <TimerDisplay
                    updateValue={this.updateTimeValue.bind(this)}
                    started={started}
                    minutes={time.minutes}
                    seconds={time.seconds}
                />
            </div>
        );
    }

    // Type is minutes or seconds and value is for setting the specified value manually
    updateTimeValue(e, type, value) {
        let {time, started} = this.state;
        if (!started) {
            time[type] = (value >= 0) ? value : this.scrollValue(e, time[type]);
            if (type === 'seconds' && time[type] >= 60) {time[type] = 59;}
            this.setState({time: time});
        }
    }

    // Takes a mouse scroll wheel event and adjusts a value up/down accordingly
    scrollValue(e, value) {
        if (value > 0 && e.deltaY > 0) {
            value--;
        } else if (e.deltaY < 0) {
            value++;
        }
        return value;
    }

    // Toggle the active status of the timer
    toggleTimer() {
        let {started, active, time, stored_time} = this.state;
        stored_time = (started) ? stored_time : {minutes: time.minutes, seconds: time.seconds};

        this.setState({
            started: true,
            active: !active,
            stored_time: stored_time
        });
        this.clearTimeout();
        this.timeout = setTimeout(this.decrementTimer.bind(this), 1000);
    }

    // Resets everything to when the timer was last started (when started last equaled false)
    resetTimer() {
        let {started, stored_time} = this.state;
        if (started) {
            this.setState({started: false, active: false, time: stored_time});
            this.clearTimeout();
        }
    }

    // Clears all the timeouts we set
    clearTimeout() {
        if (this.timeout) {clearTimeout(this.timeout);}
    }

    // Decrements the timer and plays the alarm when it reaches 0
    decrementTimer() {
        let {active, time} = this.state;
        if (active && (time.seconds > 0 | time.minutes > 0)) {
            time.seconds--;
            if (time.seconds <= 0 && time.minutes != 0) {
                time.minutes--;
                time.seconds = 59;
            }
        }

        if (time.seconds == 0 && time.minutes == 0) {
            active = false;
            this.alarm.play();
        } else {
            this.timeout = setTimeout(this.decrementTimer.bind(this), 1000);
        }

        this.setState({time: time, active: active});
    }
}

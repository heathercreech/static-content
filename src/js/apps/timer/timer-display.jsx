import React from 'react';


// Displays a value with an optional function applied
class TimeSlot extends React.Component {

    constructor() {
        super();
        this.state = {
            touching: false, // If the user is currently touching a number
            touch_start: null, // Coords of where the touch started
            touch_current: null // Coords of where the touch is currently
        };
    }

    render() {
        let {updateValue, display, type} = this.props;
        let value = this.getModifiedValue();
        return (
            <span
                onWheel={(e) => updateValue(e, type)}
                onTouchStart={(e) => this.setState({touching: true, touch_start: e.touches[0].clientY})}
                onTouchEnd={this.onTouchEnd.bind(this)}
                onTouchMove={this.onTouchMove.bind(this)}
            >
                {(display) ? display(value) : value}
            </span>
        );
    }

    // Update our current touch position
    onTouchMove(e) {
        let touch = e.touches[0];
        if (this.state.touching) {
            this.setState({touch_current: touch.clientY});
        }
    }

    // Resets the state and updates our container's state if we need to
    onTouchEnd(e) {
        let {updateValue, type} = this.props;
        this.setState({touching: false, touch_start: null, touch_current: null});
        updateValue(e, type, this.getModifiedValue());
    }

    // Dynamically modifies the value based on user's touch input
    // Interval is how far a user has to drag for it to modify the value by 1
    getModifiedValue() {
        let {touching, touch_start, touch_current} = this.state;
        let {interval, started, value, upper} = this.props;
        if (!started && touching && touch_current) {
            let difference = touch_start - touch_current;
            value = value + Math.floor(difference / interval);
            value = (value <= 0) ? 0 : value;
            value = (upper && value >= upper) ? upper : value;
        }
        return value;
    }
}

// Displays the minutes and seconds
export const TimerDisplay = props => (
    <div className="vertical-align-middle no-select">
        <TimeSlot type='minutes' interval={20} started={props.started} updateValue={props.updateValue} value={props.minutes} />:
        <TimeSlot type='seconds' upper={59} interval={10} started={props.started} value={props.seconds}
            updateValue={props.updateValue}
            display={(val) => {
                return (val < 10) ? '0' + val : val;
            }}
        />
    </div>
);

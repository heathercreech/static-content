import React from 'react';


// Generates a UI button with defined text, classes, and onClick
const UIButton = props => (
    <a onScroll={props.onScroll} className={'button naked aluminum-hover text-large text-bold shadow-thin inner-hover ' + props.className} onClick={props.onClick} style={{width: 100 + 'px'}}>
        {props.text}
    </a>
);

// Defines our timer's controls -- A start and reset button
export const TimerControls = props => (
    <div className='absolute full-width text-largest' style={{bottom: 50 + 'px'}}>
        <UIButton
            className={'margin-right ' + ((props.active) ? 'dark-red' : 'dark-green') }
            text={(props.active) ? 'Stop' : 'Start'}
            onClick={props.start}
        />
        <UIButton className='dusk' text='Reset' onClick={props.reset} />
    </div>
);

import React from 'react';
import ReactDOM from 'react-dom';
import {Timer} from './timer.jsx';

export default {
    run: function() {
        ReactDOM.render(<Timer/>, document.getElementById('timer'));
    }
};

import React from 'react';
import {HomePage} from './views/home.jsx';

export class InnyContainer extends React.Component {
    render() {
        return (
            <div className="size-page twilight text-aluminum">
                <HomePage />
            </div>
        );
    }
}

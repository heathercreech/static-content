import React from 'react';
import ReactDOM from 'react-dom';
import {InnyContainer} from './inny-container.jsx';

export default {
    run: function() {
        ReactDOM.render(<InnyContainer />, document.getElementById('inny'));
    }
};

import React from 'react';
import _ from 'underscore';

import {BorderButton} from 'lib/ui/button.jsx';


// Helper class so we don't have to define the className's every time
const MenuButton = props => (
    <div className='flex-container justify-center flex-sm-6'>
        <BorderButton
            {...props}
            classes='flex-sm-6 text-large text-bold'
        />
    </div>
);


// Contains our menu logic (when to display, hiding, menu options, etc...)
export class Menu extends React.Component {
    constructor(props) {
        super(props);
        this.state = {hidden: this.props.hidden};
    }

    render() {
        _.bindAll(this, '_toggle');
        if (!this.state.hidden) {
            return (
                <div className='absolute page-width page-height flex-container justify-center align-center column'>
                    <MenuButton text='Add' hover='dark-green' />
                    <MenuButton text='Remove' hover='dark-red' />
                    <MenuButton text='Close' hover='black' onClick={this._toggle} />
                </div>
            );
        } else {
            return (
                <div className='absolute page-width page-height flex-container justify-center align-center column'>
                    <MenuButton text='Show' hover='black' onClick={this._toggle} />
                </div>
            );
        }
    }

    _toggle() {
        this.setState({hidden: !this.state.hidden});
    }
}

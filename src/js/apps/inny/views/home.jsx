import React from 'react';

import {Menu} from './menu.jsx';


export class HomePage extends React.Component {
    constructor() {
        super();
        this.state = {
            currentPage: null
        };
    }

    render() {
        return (
            <div className="page-width page-height dark-purple">
                <Menu hidden={false} />
            </div>
        );
    }
}

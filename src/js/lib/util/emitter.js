import _ from 'underscore';


// Handles triggering functions on user-supplied events
export class Emitter {
    constructor(labels) {
        this.events = new Map();
        this.registerEvents(labels);
        console.warn('Instantiated');
    }

    // Adds some new events to the emitter
    // used by classes to add custom events to an emitter
    registerEvents(labels) {
        _.each(labels, (label) => {
            // We don't want to override the event functions if we already
            // have it registered!
            if (!this.events.has(label)) {
                this.events.set(label, []);
            }
        });
    }

    // Registeres a function to an event
    on(label, func) {
        if (this.events.has(label)) {
            this.events.get(label).push(func);
        } else {
            throw `Event "${label}" is not registered with event emitter.`;
        }
    }

    // Triggers an event and all of the associated functions
    trigger(label, that, data={}) {
        if (this.events.has(label)) {
            _.each(this.events.get(label), (e_func) => {
                e_func.bind(that)(data);
            });
        } else {
            throw `Event "${label}" is not registered with event emitter.`;
        }
    }
}

export let emitter = new Emitter([]);

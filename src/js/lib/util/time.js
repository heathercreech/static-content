// Some some time modification functions

class Time {
    hms(seconds) {
        let hours = seconds / 60 / 60;
        let ms = this.ms(hours % 1 * 60 * 60);
        return this.obj(hours, ms.minutes, ms.seconds);
    }

    ms(seconds) {
        let minutes = seconds / 60;
        seconds = (minutes % 1) * 60;
        return this.obj(0, minutes, seconds);
    }

    obj(h, m ,s) {
        return {
            hours: Math.floor(h),
            minutes: Math.floor(m),
            seconds: Math.floor(s)
        };
    }

    objToSeconds(obj) {
        return obj.hours * 60 * 60 + obj.minutes * 60 + obj.seconds;
    }

    strToSeconds(str) {
        let parts = str.split(':');
        let time = (parts.length == 2) ? this.obj(0, parts[0], parts[1]) : this.obj(parts[0], parts[1], parts[2]);
        return this.objToSeconds(time);
    }
}

export default new Time();

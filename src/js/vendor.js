global.jQuery = require('jquery');
global.$ = global.jQuery;

import * as caseMods from 'change-case';
for (let caseMod in caseMods) {
    String.prototype[caseMod] = function() {return caseMods[caseMod](this);};
}

import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, text, number } from '@storybook/addon-knobs';
import { Truncate } from '.';

storiesOf('atom.Truncate', module)
    .addDecorator(withKnobs)
    .add('default', () => (
        <Truncate limit={number('Limit', 50, {max: 500})}>
            { text('Text', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam volutpat vehicula maximus. Vivamus ut tortor ut augue auctor consequat. Nunc rutrum sapien mattis hendrerit hendrerit. Mauris lacinia lorem tortor, eget cursus lectus iaculis ac. Ut mauris enim, iaculis sit amet nunc in, venenatis pellentesque sem. Morbi efficitur nunc felis, eu porttitor ipsum imperdiet quis. Proin pulvinar a orci vel fringilla. Integer ornare nisi id dignissim dapibus. Nulla tempus, massa a accumsan venenatis, velit risus cursus eros, vel laoreet lacus nunc vitae nisi. Sed molestie tempus venenatis. Integer vehicula ipsum sed risus facilisis feugiat. Maecenas fermentum tempor viverra.') }
        </Truncate>
    ));

import React from 'react';
import PropTypes from 'prop-types';
import truncate from 'truncate';


export const Truncate = ({ children, limit=50 }) => (
    <div>{ truncate(children, limit) }</div>
);
Truncate.propTypes = { children: PropTypes.string.isRequired, limit: PropTypes.number };

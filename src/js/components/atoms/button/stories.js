import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import { Button, ToggleButton } from '.';

storiesOf('button', module)
    .addDecorator(withKnobs)
    .add('Button', () => (
        <Button onClick={() => {}}>
            { text('Text', 'Click') }
        </Button>
    ))
    .add('ToggleButton', () => (
        <ToggleButton on='Hello' onClick={() => {}}>
            { text('Text', 'Click') }
        </ToggleButton>
    ));

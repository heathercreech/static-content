import React from 'react';
import PropTypes from 'prop-types';
import { Toggleable } from 'atoms/toggleable';


export const Button = ({ children, onClick, ...rest }) => (
    <button onClick={ onClick } { ...rest }>{ children }</button>
);
Button.propTypes = { children: PropTypes.string.isRequired, onClick: PropTypes.func.isRequired };


export const ToggleButton = ({ children, on, onClick, ...rest }) => (
    <Toggleable
        render={(toggled, toggle) => (
            <Button onClick={() => { onClick(); toggle(); }} { ...rest }>
                { (!toggled) ? children : on }
            </Button>
        )}
    />
);

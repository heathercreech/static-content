import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'atoms/button';
import { Toggleable } from 'atoms/toggleable';
import { Hoverable } from 'atoms/hoverable';


export const PlayButton = ({ onClick, ...rest }) => (
    <Toggleable render={(playing, toggle) => (
        <Hoverable render={(hovered, hover) => (
            <Button
                onMouseEnter={ () => hover(true) }
                onMouseLeave={ () => hover(false) }
                onClick={ () => { toggle(); onClick(); }} { ...rest }
            >
                { (playing)
                    ? (hovered) ? '=' : ')))'
                    : '>'
                }
            </Button>
        )} />
    )} />
);
PlayButton.propTypes = { onClick: PropTypes.func.isRequired };


import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs';
import { PlayButton } from '.';

storiesOf('button', module)
    .addDecorator(withKnobs)
    .add('PlayButton', () => (
        <PlayButton onClick={() => {}} />
    ));

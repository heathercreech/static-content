import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, text, color } from '@storybook/addon-knobs';
import { Bar } from '.';

storiesOf('Bar', module)
    .addDecorator(withKnobs)
    .add('default', () => (
        <div>
            <Bar
                width={text('Width', '50%')}
                height={text('Height', '2rem')}
                color={color('Color', 'lightcoral')}
            />
        </div>
    ));

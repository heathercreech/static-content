//import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

export const Bar = styled.div`
    width: ${({width}) => (width) ? width : '100%' };
    height: ${({height}) => (height) ? height : '50px'};
    background-color: ${({color}) => color}
`;
Bar.propTypes = {
    children: PropTypes.node,
    width: PropTypes.string,
    height: PropTypes.string,
    color: PropTypes.string
};


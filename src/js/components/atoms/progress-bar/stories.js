import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, color, number } from '@storybook/addon-knobs';
import { ProgressBar } from '.';

storiesOf('Progress Bar', module)
    .addDecorator(withKnobs)
    .add('default', () => (
        <div>
            <ProgressBar
                progress={ number('Progress', 0.50, {
                    range: true,
                    min: 0,
                    max: 1,
                    step: 0.025
                }) }
                bgColor={ color('Background Color', 'lightgrey') }
                fgColor={ color('Foreground Color', 'lightcoral') }
            />
        </div>
    ));

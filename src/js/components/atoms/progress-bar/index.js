import React from 'react';
import PropTypes from 'prop-types';
import { Bar } from 'atoms/bar';


export const ProgressBar = ({ progress, bgColor, fgColor, ...rest }) => (
    <Bar color={ bgColor }>
        <Bar color={ fgColor } width={ `${progress * 100}%` } { ...rest } />
    </Bar>
);
ProgressBar.propTypes = {
    progress: (props, propName, componentName) => {
        let MAX = 1, MIN = 0;
        let value = props[propName];
        if (typeof value != 'number') {
            return new Error(`Invalid prop ${propName} supplied to component ${componentName}, expected number but got ${typeof value}`);
        } else if (value < MIN || value > MAX) {
            return new Error(`
                Invalid prop ${propName} supplied to component ${componentName},
                value (${(value < MIN) ? 'less' : 'more'})
                than ${(value < MIN) ? `MIN (${MIN})` : `MAX (${MAX})`}
            `);
        }
    },
    bgColor: PropTypes.string,
    fgColor: PropTypes.string
};


import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import { Toggleable } from '.';

storiesOf('Toggleable', module)
    .addDecorator(withKnobs)
    .add('default', () => (
        <Toggleable render={
            (toggled, toggle) => (
                <button onClick={toggle}>
                    { (toggled)
                        ? text('On Text', 'On')
                        : text('Off Text', 'Off')
                    }
                </button>
            )
        } />
    ));

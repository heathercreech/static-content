import React from 'react';
import PropTypes from 'prop-types';


export class Toggleable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {toggled: false};
    }

    toggle() {
        this.setState(previousState => ({ toggled: !previousState.toggled }));
    }

    render() {
        let { render } = this.props;
        return render(this.state.toggled, this.toggle.bind(this));
    }
}
Toggleable.propTypes = { render: PropTypes.func.isRequired };

import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import { Hoverable } from '.';

storiesOf('Hoverable', module)
    .addDecorator(withKnobs)
    .add('default', () => (
        <Hoverable
            render={ (hovered, handleHover) => (
                <span onMouseEnter={() => handleHover(true)} onMouseLeave={() => handleHover(false)}>
                    {(hovered)
                        ? text('Hovered Text', 'Hovered')
                        : text('Text', 'Not Hovered')
                    }
                </span>
            )}
        />
    ));

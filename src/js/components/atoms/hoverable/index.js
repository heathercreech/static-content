import React from 'react';
import PropTypes from 'prop-types';


export class Hoverable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hovered: false
        };
    }

    // Having the user provied a value allows them to hover/unhover on different elements within a component
    // E.g. a button within a div, we can produce the hover effect when the mouse first hovering the button
    // but unhover after leaving the div
    handleHover(hovered) {
        this.setState({ hovered: hovered });
    }

    render() {
        return this.props.render(this.state.hovered, this.handleHover.bind(this));
    }
}
Hoverable.propTypes = { render: PropTypes.string.isRequired };

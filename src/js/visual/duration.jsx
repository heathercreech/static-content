import React from 'react';

import Time from 'lib/util/time.js';


export class Duration extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        let {current, total} = this.props;
        total = this._format(total);
        current = this._format(current, total.obj.hours > 0);

        return (
            <span>{current.text} / {total.text}</span>
        );
    }

    _msOrHms(seconds) {
        return (this.props.total / 60 / 60 < 1) ? Time.ms(seconds) : Time.hms(seconds);
    }

    _format(seconds, showHours) {
        let obj = this._msOrHms(seconds);
        let s = this._zeroify(obj.seconds);
        let m = this._zeroify(obj.minutes);
        let h = (this.props.zeroifyHours) ? this._zeroify(obj.hours) : obj.hours;

        return {
            text: (obj.hours == 0 && !showHours) ? [m, s].join(':') : [h, m, s].join(':'),
            obj: obj
        };
    }

    _zeroify(value) {
        return (value < 10) ? '0' + value : value;
    }
}


import React from 'react';
import PropTypes from 'prop-types';


export const Page = ({children, className='twilight text-aluminum'}) => (
    <div className={className}>
        <div className="size-page" style={{'overflowY': 'auto'}}>
            {children}
        </div>
    </div>
);


Page.propTypes = {
    children: PropTypes.node.isRequired
};

import React from 'react';
import PropTypes from 'prop-types';


export const Bar = ({ height, width, children, ...rest }) => (
    <div style={{ height, width }} { ...rest }>
        { children }
    </div>
);
Bar.defaultProps = { height: '50px', width: '100%' };
Bar.propTypes = { height: PropTypes.string, width: PropTypes.string, children: PropTypes.node };

export const ProgressBar = ({ percent, fillColor, emptyColor, children, ...rest }) => (
    <div className='relative'>
        <Bar className={emptyColor} { ...rest } />
        <Bar
            width={`${percent * 100}%`}
            className={`absolute hug-top ${fillColor}`}
            { ...rest }
        />
        <Bar className='absolute hug-top' children={children} {...rest} />
    </div>
);
ProgressBar.propTypes = {
    percent: PropTypes.number.isRequired,
    fillColor: PropTypes.string.isRequired,
    emptyColor: PropTypes.string.isRequired,
    children: PropTypes.node
};

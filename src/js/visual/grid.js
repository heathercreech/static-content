import React from 'react';
import PropTypes from 'prop-types';


export const Grid = ({justify, align, children, className, ...rest}) => (
    <div className={`
        flex-container
        ${(justify) ? `justify-${justify}` : ''}
        ${(align) ? `align-${align}` : ''}
        ${className}
    `} {...rest}>
        {children}
    </div>
);
Grid.defaultProps = { className: '', justify: '', align: '' };
Grid.propTypes = { children: PropTypes.node };

export const GridItem = ({small='', medium='', large='', className='', children, ...rest}) => (
    <div className={`
        ${small ? `flex-sm-${small}` : ''}
        ${medium ? `flex-md-${medium}` : ''}
        ${large ? `flex-lg-${large}` : ''}
        ${className}
    `} {...rest}>
        {children}
    </div>
);


export const FullHeightGrid = ({ className, ...rest }) => (
    <Grid className={`full-height ${className}`} { ...rest } />
);
FullHeightGrid.defaultProps = { className: '' };
FullHeightGrid.propTypes = { className: PropTypes.string };

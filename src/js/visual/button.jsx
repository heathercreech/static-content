import React from 'react';


// A base button template
export const Button = props => (
    <a
        className={`
            button
            ${(props.hover) ? props.hover + '-hover' : ''}
            ${props.color || '' }
            ${props.classes}
        `}
        onClick={props.onClick}
        onScroll={props.onScroll}
    >
        {props.text}
    </a>
);

// Button with some easier to type border options
export const BorderButton = props => (
    <Button
        {...props}
        classes={`
            ${(props.border) ? 'border-' + props.border : 'border-normal'}
            ${(props.borderColor) ? 'border-' + props.borderColor : ''}
            ${props.classes}
        `}
    />
);

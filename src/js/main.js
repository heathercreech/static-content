import 'babel-polyfill';
import * as apps from 'glob:./apps/**/app.js';


// Repackage our imported apps so that we can call them by name
// e.g. 'reactApps.inny.run();'
let restructured = {};
for (var appName in apps) {
    let modName = '';
    if (appName.includes('$')) {
        modName = appName.split('$')[0];
    }
    restructured[modName] = apps[appName];
}

global.apps = restructured;

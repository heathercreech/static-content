import Backbone from 'backbone';
import videojs from 'video.js';

export default Backbone.View.extend({
    initialize: function() {
        this.player = videojs('video', {});
        this._matchWindow();

        let that = this;
        $(window).resize(function() {that._matchWindow();});
        this.player.play();

        this._overlay({});
    },

    render: function() {},

    _overlay: function() {
        let $player = $(this.player.el());
        let $controls = $player.find('.vjs-control-bar');
        $controls.append(render('products/experiments/video/_source-button.html'));
    },

    //matches the windows width and height
    _matchWindow() {
        this.player.height($(window).height());
        this.player.width($(window).width());
    }
});

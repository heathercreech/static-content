import Backbone from 'backbone';

export default Backbone.View.extend({
    initialize: function(options) {
        $('#source-button').click(function() {
            $el.toggleClass('vjs-hidden');
        });

        this.templatePath = 'products/experiments/video/_source-panel.html';
        this.$vsrcEl = $('#video-source');
        this.player = options.player;
        this.render();

        let that = this;
        $('.source-button').click(function() {that.render();});
    },

    render: function() {
        this.src = $('.source-input').val();
        if (this.src && this.src.length > 0) {
            this.player.src(this.src);
        }
        $('#video-source').prop('src', this.src);
        this.$el.html(render(this.templatePath));
        this.$el.toggleClass('vjs-hidden');
    }
});

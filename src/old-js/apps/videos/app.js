import player from './views/player';
import source from './views/source';

export default {
    run: function() {
        let playerView = new player({});
        new source({el: '#source-panel', player: playerView.player});
    }
};

import tabview from './views/tabview.js';

export default {
    init: function(id) {
        new tabview({
            tabViewId: id
        });
    }
};

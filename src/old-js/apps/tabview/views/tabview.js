import Backbone from 'backbone';

export default Backbone.View.extend({
    initialize: function(options) {
        this.tabViewId = options.tabViewId;
        this.$container = $('#tab-view-' + this.tabViewId);
        this.$contentContainer = this.$container.find('#tab-container-' + this.tabViewId);
        this.$tabsControls = this.$container.find('.tab-control');

        this.currentLink = $('.tab-control.active').data('tab-link');
        this.render();

        let that = this;
        this.$tabsControls.click(function() {
            let link = $(this).data('tab-link');
            if (link !== that.currentLink) {
                that._updateLink(link);
            }
        });
    },

    render: function() {
        this.$contentContainer.height(this.$container.height() - this.$tabsControls.height());
    },

    //Hide the current link, update with new link, show new one
    _updateLink(link) {
        this._toggleCurrentLink();
        this.currentLink = link;
        this._toggleCurrentLink();
    },

    _toggleCurrentLink() {
        let dataSelector = '[data-tab-link="' + this.currentLink + '"';
        $('div' + dataSelector).toggleClass('hidden');
        $('a' + dataSelector).toggleClass('active');
    }
});

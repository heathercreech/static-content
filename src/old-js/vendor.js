global.jQuery = require('jquery');
global.$ = global.jQuery;

//global.uuid = require('uuid');
import uuid from 'uuid';
//var nunjucks = require('nunjucks');
var nunjucks = require('nunjucks');
let env = nunjucks.configure('/static/templates');
env.addGlobal('uuid', uuid);
global.render = env.render;

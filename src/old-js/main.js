import * as apps from 'glob:./apps/**/app.js';


let restructured = {};
for (var appName in apps) {
    let modName = '';
    if (appName.includes('$')) {
        modName = appName.split('$')[0];
    }
    restructured[modName] = apps[appName];
}

global.apps = restructured;
